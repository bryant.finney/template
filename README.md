# Template

[![Open in Visual Studio Code](https://open.vscode.dev/badges/open-in-vscode.svg)](vscode://ms-vscode-remote.remote-containers/cloneInVolume?url=https://gitlab.com/tfmod/template.git)

This repository can be used as a template for new Terraform projects.

> Originated from
> [Terraform Modules / Template](https://gitlab.com/tfmod/template/)

## Structure

This project is organized as follows:

| File Name                    | Purpose                                                                                                                                    |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| [caller.sh](caller.sh)       | this script is used to tag the individual caller (when deploying resources manually)                                                       |
| [data.tf](data.tf)           | [Terraform Data Sources](https://www.terraform.io/docs/language/data-sources/index.html) referenced by this project go here                |
| [main.tf](main.tf)           | house resources that actualize this project's main or primary function                                                                     |
| [provider.tf](provider.tf)   | the [Providers](https://www.terraform.io/docs/language/providers/index.html) for this project are housed here                              |
| [resources.tf](resources.tf) | configure secondary resources (i.e. resources on which the primary function of this project depend; typically resources such as IAM roles) |
| [variables.tf](variables.tf) | configure [Terraform Input Variables](https://www.terraform.io/docs/language/values/variables.html)                                        |

## Notes

- separating primary and secondary resources supports flexibility: as additional
  projects are created, [resources.tf](resources.tf) can be evaluated for consolidation
  of secondary resources required by this project
  - conversely, primary resources in [main.tf](main.tf) are fundamental to the purpose
    of this project and will rarely be managed in other projects

<!-- prettier-ignore-start -->
<!-- BEGIN_TF_DOCS -->

## Requirements

No requirements.

## Providers

| Name                                                            | Version |
| --------------------------------------------------------------- | ------- |
| <a name="provider_aws"></a> [aws](#provider_aws)                | 4.0.0   |
| <a name="provider_external"></a> [external](#provider_external) | 2.2.0   |

## Modules

No modules.

## Resources

| Name                                                                                                                          | Type        |
| ----------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [external_external.caller](https://registry.terraform.io/providers/hashicorp/external/latest/docs/data-sources/external)      | data source |

## Inputs

| Name                                                            | Description                                                                    | Type     | Default | Required |
| --------------------------------------------------------------- | ------------------------------------------------------------------------------ | -------- | ------- | :------: |
| <a name="input_deploy_url"></a> [deploy_url](#input_deploy_url) | for use with CI; this tag identifies the CI job used to perform the deployment | `string` | `null`  |    no    |

## Outputs

No outputs.

<!-- END_TF_DOCS -->
<!-- prettier-ignore-end -->
