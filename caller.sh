#!/bin/sh
# shellcheck shell=sh

# Summary: Provide the Terraform caller information as a JSON object.
# Author:  Bryant Finney (https://bryant-finney.github.io/about/)

name="$(id -un)"
host="$(hostname)"

printf '{"username": "%s", "hostname": "%s", "id": "%s"}\n' "$name" "$host" "$name@$host"
