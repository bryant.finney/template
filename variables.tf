# Summary: Declare variables for CLI prompts / shell environment references.
# Author:  Bryant Finney (https://bryant-finney.github.io/about/)

variable "deploy_url" {
  type        = string
  default     = null
  description = "for use with CI; this tag identifies the CI job used to perform the deployment"
}

data "external" "caller" {
  program = [
    "sh", "${path.module}/caller.sh"
  ]
}

locals {
  tags = {
    "Deploy URL" = var.deploy_url == null ? data.external.caller.result.id : var.deploy_url
    Workspace    = terraform.workspace
  }
}
